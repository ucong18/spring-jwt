package com.ucong.csi.demojwtapp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LandingPageController {

    @RequestMapping("/index")
    public String index(){
        return "Halaman Index, tidak kena authentitcation";
    }
}
